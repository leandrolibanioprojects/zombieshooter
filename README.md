# ZombieShooter 
ZombieShooter is a SFML game. It is being developed following the "Beginning C++ Game Programming" by PacktPub.

I'm providing a Code::Blocks project but you should be able to manage it i any IDE you want;

Just make sure you have SFML latest version installed;

On Debian-Based linux:
	$ sudo apt update
	$ sudo apt install libsfml-dev

On Windows:
	Install SFML to your MinGW folder.