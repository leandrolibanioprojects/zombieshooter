#include "include/Zombie.h"
#include "include/TextureHolder.h"
#include <cstdlib>
#include <ctime>

void Zombie::spawn(float startX, float startY, int type, int seed)
{
    switch(type)
    {
    case 0:
        //Bloater
        m_sprite = sf::Sprite(TextureHolder::GetTexture("assets/graphics/bloater.png"));
        m_speed = 40;
        m_health = 5;
        break;

    case 1:
        //Chaser
        m_sprite = sf::Sprite(TextureHolder::GetTexture("assets/graphics/chaser.png"));
        m_speed = 80;
        m_health = 1;
        break;

    case 2:
        //Crawler
        m_sprite = sf::Sprite(TextureHolder::GetTexture("assets/graphics/crawler.png"));
        m_speed = 20;
        m_health = 3;
        break;
    }

    //Modify the speed to make each zombie unique
    srand((int)time(0) * seed);
    float modifier = (rand() % MAX_VARRIANCE) + OFFSET;

    modifier /= 100;
    m_speed *= modifier;

    //Initalize location;
    m_position.x = startX;
    m_position.y = startY;

    m_sprite.setOrigin(25, 25);
    m_alive = true;

    //Set position
    m_sprite.setPosition(m_position);
}

bool Zombie::hit()
{
    m_health --;

    if(m_health <= 0)
    {
        //dead
        m_alive = false;
        m_sprite.setTexture(TextureHolder::GetTexture("assets/graphics/bllood.png"));

        return true;
    }

    //Not dead
    return false;
}

bool Zombie::isAlive()
{
    return m_alive;
}

sf::FloatRect Zombie::getPosition()
{
    return m_sprite.getGlobalBounds();
}

sf::Sprite Zombie::getSprite()
{
    return m_sprite;
}

void Zombie::update(float elapsedTime, sf::Vector2f playerLocation)
{
    float playerX = playerLocation.x;
    float playerY = playerLocation.y;

    //Update the zombie position variables
    if(playerX > m_position.x)
    {
        m_position.x = m_position.x +  m_speed * elapsedTime;
    }

    if(playerY > m_position.y)
    {
        m_position.y = m_position.y + m_speed * elapsedTime;
    }

    if(playerX < m_position.x)
    {
        m_position.x = m_position.x - m_speed * elapsedTime;
    }

    if(playerY < m_position.y)
    {
        m_position.y = m_position.y - m_speed * elapsedTime;
    }

    //Move the sprite
    m_sprite.setPosition(m_position);

    //Face the sprite in the correct angle
    float angle = (atan2(playerY - m_position.y, playerX - m_position.x) * 180) / 3.141;

    m_sprite.setRotation(angle);
}
