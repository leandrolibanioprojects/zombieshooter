#include <SFML/Graphics.hpp>

#include "include/Zombie.h"
#include "include/Player.h"
#include "include/main.h"
#include "include/TextureHolder.h"
#include "include/Bullet.h"

int main()
{
    TextureHolder holder;
        
    // GAME Variables
    enum class State
    {
        PAUSED, LEVEL_UP, GAME_OVER, PLAYING
    };

    //Start with GAME_OVER
    State state = State::GAME_OVER;

    //Get sreen resolution and create window
    sf::Vector2f resolution;
    resolution.x = sf::VideoMode::getDesktopMode().width;
    resolution.y = sf::VideoMode::getDesktopMode().height;
    sf::RenderWindow window(sf::VideoMode(resolution.x, resolution.y), "ZombieShooter", sf::Style::Fullscreen);

    //View for main action
    sf::View mainView(sf::FloatRect(0, 0, resolution.x, resolution.y));

    //Clock for timing everything
    sf::Clock clock;

    //PLAYING time
    sf::Time gameTimeTotal;

    //Mouse in relation to world
    sf::Vector2f mouseWorldPosition;
    //Mouse in relation to screen
    sf::Vector2i mouseScreenPosition;

    //Instance of Player;
    Player player;

    //The boundaries of the arena
    sf::IntRect arena;

    //Create the background Vertex Array and load the texture
    sf::VertexArray background;

    sf::Texture textureBG = TextureHolder::GetTexture("assets/graphics/background_sheet.png");

    //Prepare a horde of zombies
    int numZombies;
    int numZombiesAlive;

    Zombie* zombies = nullptr;

    Bullet bullets[100];
    int currentBullet = 0;
    int bulletSpare = 24;
    int bulletsInClip = 6;
    int clipSize = 6;
    float fireRate = 1;
    sf::Time lastPressed;

    //Hide mouse pointer and  replace it with the crosshair image
    window.setMouseCursorVisible(false);
    sf::Sprite crossHair;
    sf::Texture crossHairTexture = TextureHolder::GetTexture("assets/graphics/crosshair.png");

    crossHair.setTexture(crossHairTexture);
    crossHair.setOrigin(25, 25);

    // GAME VARIABLES

    //The main game loop
    while(window.isOpen())
    {
        /*=================================================================
        HANDLE INPUT
        =================================================================*/

        // Handle events by polling
        sf::Event event;
        while (window.pollEvent(event))
        {
            if(event.type == sf::Event::KeyPressed)
            {
                //Pause a game while playing
                if(event.key.code == sf::Keyboard::Return && state == State::PLAYING)
                {
                    state = State::PAUSED;
                }
                //Restart while playing
                else if(event.key.code == sf::Keyboard::Return && state == State::PAUSED)
                {
                    state = State::PLAYING;
                    //Reset the clock;
                    clock.restart();
                }
                //Start a new game
                else if(event.key.code == sf::Keyboard::Return && state == State::GAME_OVER)
                {
                    state = State::LEVEL_UP;
                }

                if(state == State::PLAYING)
                {
                    //Realod
                    if(event.key.code == sf::Keyboard::R)
                    {
                        if(bulletSpare >= clipSize)
                        {
                            //Plenty of bullets
                            bulletsInClip = clipSize;
                            bulletSpare -= clipSize;
                        }
                        else if(bulletSpare > 0)
                        {
                            //Only a few bullets
                            bulletsInClip = bulletSpare;
                            bulletSpare = 0;
                        }
                        else
                        {
                            //?
                        }
                    }
                }
            }
        } // End of polling

        // Handle player quitting
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            window.close();
        }
        //

        // Handle WASD while playing
        if(state == State::PLAYING)
        {
            //Handle the pressing and releasing the WASD keys
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
                player.moveUp();
            else
                player.stopUp();

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
                player.moveDown();
            else
                player.stopDown();

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
                player.moveLeft();
            else
                player.stopLeft();

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
                player.moveRight();
            else
                player.stopRight();

            //Fire a bullet
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                if(gameTimeTotal.asMilliseconds() - lastPressed.asMilliseconds() >
                    1000 / fireRate && bulletsInClip > 0)
                {
                    //Pass the center of the player and the center of the crosshair
                    bullets[currentBullet].shoot(
                        player.getCenter().x, player.getCenter().y,
                        mouseWorldPosition.x, mouseWorldPosition.y
                    );

                    currentBullet ++;

                    if(currentBullet > 99)
                    {
                        currentBullet = 0;
                    }

                    lastPressed = gameTimeTotal;
                    bulletsInClip --;
                }
            }//End of fire a bullet;

        }// End WASD while playing

        // Handle the player leveling up
        if(state == State::LEVEL_UP)
        {

            if(event.key.code == sf::Keyboard::Num1)
            {
                state = State::PLAYING;
            }
            if(event.key.code == sf::Keyboard::Num2)
            {
                state = State::PLAYING;
            }
            if(event.key.code == sf::Keyboard::Num3)
            {
                state = State::PLAYING;
            }
            if(event.key.code == sf::Keyboard::Num4)
            {
                state = State::PLAYING;
            }
            if(event.key.code == sf::Keyboard::Num5)
            {
                state = State::PLAYING;
            }
            if(event.key.code == sf::Keyboard::Num6)
            {
                state = State::PLAYING;
            }

            if(state == State::PLAYING)
            {
                //Prepare the arena
                //TODO: Modify next two lines
                arena.width = 1000;
                arena.height = 1000;
                arena.top = 0;
                arena.left = 0;

                //Pass background array by reference to createBackground function
                int tileSize = createBackground(background, arena);

                //Spawn the player in the middle of the arena
                player.spawn(arena, resolution, tileSize);

                //Create a horde of zombies
                numZombies = 100;
                //Delete the prevously allocated memory
                delete[] zombies;
                zombies = createHorde(numZombies, arena);
                numZombiesAlive = numZombies;

                //Reset the clock
                clock.restart();
            }

        }// End of leveling up

        /*=================================================================
        UPDATE THE FRAME
        =================================================================*/

        // Update the scene
        if(state == State::PLAYING)
        {
            //Update the deltaTime
            sf::Time dt = clock.restart();
            //Update the total game time
            gameTimeTotal += dt;

            //Make a decimal fraction of 1 from the dt
            float dtAsSeconds = dt.asSeconds();

            //Where is the mouse pointer
            mouseScreenPosition = sf::Mouse::getPosition(window);
            //Convert mouse position to world coordinates
            mouseWorldPosition = window.mapPixelToCoords(sf::Mouse::getPosition(), mainView);

            crossHair.setPosition(mouseWorldPosition);

            //Update player
            player.update(dtAsSeconds, sf::Mouse::getPosition());

            //Make a note of player's new position
            sf::Vector2f playerPosition(player.getCenter());
            //Make the view centre around the player
            mainView.setCenter(player.getCenter());

            //Loop through each zombie and update them
            for(int i = 0; i < numZombies; i++)
            {
                if(zombies[i].isAlive())
                {
                    zombies[i].update(dt.asSeconds(), playerPosition);
                }
            }

            //Update any bullets that are in flight
            for(int i = 0; i < 100; i++)
            {
                if(bullets[i].isInFlight())
                {
                    bullets[i].update(dtAsSeconds);
                }
            }

        } // End update the scene

        /*=================================================================
        DRAW THE SCENE
        =================================================================*/

        // Draw everything
        if(state == State::PLAYING)
        {
            window.clear();

            //Set main view to window and draw everything related to it
            window.setView(mainView);

            //Draw the backgrund
            window.draw(background, &textureBG);

            //Draw the zombies
            for(int i = 0; i < numZombies; i++)
            {
                window.draw(zombies[i].getSprite());
            }

            for(int i = 0; i < 100; i++)
            {
                if(bullets[i].isInFlight())
                {
                    window.draw(bullets[i].getShape());
                }
            }

            //Draw the player
            window.draw(player.getSprite());

            //Draw the crosshair
            window.draw(crossHair);
        }
        if(state == State::PAUSED)
        {

        }
        if(state == State::GAME_OVER)
        {

        }
        if(state == State::LEVEL_UP)
        {

        } // End draw

        window.display();

        //Enf of game loop
    }

    delete[] zombies;

    return 0;
}