#include "include/main.h"
#include "include/Zombie.h"

Zombie* createHorde(int numZombies, sf::IntRect arena)
{
    Zombie* zombies = new Zombie[numZombies];

    int maxY = arena.height - 20;
    int minY = arena.top + 20;
    int maxX = arena.width - 20;
    int minX = arena.left + 20;

    for(int i = 0; i < numZombies; i++)
    {
        //Wich side should the zombie spawn
        srand((int)time(0) * i);
        int side = (rand() % 4);

        float x, y;

        switch(side)
        {
        case 0:
            //Left
            x = minX;
            y = (rand() % maxY + minY);
            break;
        case 1:
            //Right
            x = maxX;
            y = (rand() % maxY + minY);
            break;
        case 2:
            //Top
            y = minY;
            x = (rand() % maxX + minX);
            break;
        case 3:
            //Bottom
            y = minY;
            x = (rand() % maxX + minX);
            break;
        }

        //Bloater, Chaser or Crawler
        srand((int)time(0) * i * 2);
        int type = (rand() % 3);

        //Spawn the new zombie into array
        zombies[i].spawn(x, y, type, i);
    }
    return zombies;
}
