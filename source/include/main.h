#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include "Zombie.h"

int createBackground(sf::VertexArray& r_vA, sf::IntRect arena);

Zombie* createHorde(int numZombies, sf::IntRect arena);

#endif // MAIN_H_INCLUDED
