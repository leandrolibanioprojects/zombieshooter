#ifndef TEXTUREHOLDER_H_INCLUDED
#define TEXTUREHOLDER_H_INCLUDED

#include<SFML/Graphics.hpp>
#include<map>

class TextureHolder
{
private:
    //Map conainer to hold string an Texture pairs
    std::map<std::string, sf::Texture> m_textures;

    //A static pointer to the class itself
    static TextureHolder* m_s_instance;
public:
    //Constructor
    TextureHolder();
    static sf::Texture& GetTexture(std::string const& fileName);
};

#endif // TEXTUREHOLDER_H_INCLUDED
