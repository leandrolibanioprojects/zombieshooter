#include<SFML/Graphics.hpp>

class Bullet
{
private:
    sf::Vector2f m_position; //Center Position
    
    sf::RectangleShape m_shape; //Shape

    bool m_inFlight = false; //Is the bullet flying?

    float m_speed = 1000; //How fast does the bullet move?

    float m_distanceX; //Ammount in pixels to fly, horizontaly and vertically
    float m_distanceY;

    //Boundaries
    float m_maxX;
    float m_maxY;
    float m_minX;
    float m_minY;

public:
    Bullet();

    void stop(); //Stop the bullet
    bool isInFlight(); //IS the bullet flying?
    void shoot(float startX, float startY, float xTarget, float yTarget); //Launch a new bullet
    sf::FloatRect getPosition(); //Get the bullet bounds, for collision
    sf::RectangleShape getShape(); //Get the shape for drawing

    void update(float elapsedTime); //Update the bullet every frame
};