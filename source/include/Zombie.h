#ifndef ZOMBIE_H_INCLUDED
#define ZOMBIE_H_INCLUDED

#include <SFML/Graphics.hpp>

class Zombie
{
private:
    //How fast is each zombie type?
    const float BLOATER_SPEED = 40;
    const float CHASER_SPEED = 80;
    const float CRAWLER_SPEED = 20;

    //How tough is eaxh zombie type?
    const float BLOATER_HEALTH = 5;
    const float CHASER_HEALTH = 1;
    const float CRAWLER_HEALTH = 3;

    //Make each zombie vary it's speed
    const int MAX_VARRIANCE = 30;
    const int OFFSET = 101 - MAX_VARRIANCE;

    //Where is this zombie?
    sf::Vector2f m_position;

    //A sprite
    sf::Sprite m_sprite;

    //How fast can this one run
    float m_speed;

    //How much health has it got
    float m_health;

    //Alive?
    bool m_alive;

public:
    //Handle bullet hit zombie
    bool hit();

    //Is it alive?
    bool isAlive();

    //Spawn a new Zombie
    void spawn(float startX, float startY, int type, int seed);

    //Return a rectangle that is the world position
    sf::FloatRect getPosition();

    //Get a copy of the sprite to drawn
    sf::Sprite getSprite();

    //Update the zombie each frame
    void update(float elapsedTime, sf::Vector2f playerLocation);
};

#endif // ZOMBIE_H_INCLUDED
