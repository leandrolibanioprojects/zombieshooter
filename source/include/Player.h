#ifndef PLAYER_H
#define PLAYER_H

#include <SFML/Graphics.hpp>

class Player
{
    public:
        Player();
        virtual ~Player();

        void spawn(sf::IntRect arena, sf::Vector2f resolution, int tileSize);

        //End of the game
        void resetPlayerStats();
        //Getting hit by zombies
        bool hit(sf::Time timeHit);
        //How long ago was the last hit
        sf::Time getLastHitTime();
        //Where is the Player
        sf::FloatRect getPosition();
        //Where is the center of the player
        sf::Vector2f getCenter();
        //Angle of facing
        float getRotation();
        //Get Sprite
        sf::Sprite getSprite();

        //The nest four move the player
        void moveLeft();
        void moveUp();
        void moveDown();
        void moveRight();

        //Stop the player movement
        void stopLeft();
        void stopUp();
        void stopDown();
        void stopRight();

        //Call every frame
        void update(float elapsedTime, sf::Vector2i mousePosition);

        //Speed Boost
        void upgradeSpeed();
        //Health Boost
        void upgradeHealth();
        //Maximum Health Boost
        void increaseHealthLevel(int amount);

        //Get Health
        int getHealth();

    protected:

    private:
        const float START_SPEED = 200;
        const float START_HEALTH = 100;

        //Where is the Player
        sf::Vector2f m_position;

        //Sprite
        sf::Sprite m_sprite;
        //Texture
        sf::Texture m_texture;

        //Screen Resolution
        sf::Vector2f m_resolution;

        //Size of arena
        sf::IntRect m_arena;

        //Size of tiles
        int m_tileSize;

        //Directions
        bool m_upPressed;
        bool m_downPressed;
        bool m_leftPressed;
        bool m_rightPressed;

        //Current Health
        int m_health;

        //Maximum health
        int m_maxHealth;

        //Time of last hit
        sf::Time m_lastHit;

        //Speed in px per second
        float m_speed;
};

#endif // PLAYER_H
