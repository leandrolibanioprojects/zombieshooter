#include "include/Bullet.h"

Bullet::Bullet()
{
    m_shape.setSize(sf::Vector2f(2, 2));
}

void Bullet::shoot(float startX, float startY, float xTarget, float yTarget)
{
    m_inFlight = true;

    m_position.x = startX;
    m_position.y = startY;

    //The gradient of flight path
    float gradient = (startX - xTarget) / (startY - yTarget);

    if(gradient < 0)
    {
        gradient = -gradient;
    }

    //The ratio between x and y
    float ratioXY = m_speed / (1 + gradient);

    //Set the 'speed' horizontally and vertically
    m_distanceY = ratioXY;
    m_distanceX = ratioXY * gradient;

    //Point the bullet in the right direction
    if(xTarget < startX)
    {
        m_distanceX = -m_distanceX;
    }

    if(yTarget < startY)
    {
        m_distanceY = -m_distanceY;
    }

    //Max range = 1000

    float range = 1000;

    m_minX = startX - range;
    m_maxX = startX + range;
    m_minX = startY - range;
    m_maxY = startY + range;

    //Position the bullet ready to be drawn
    m_shape.setPosition(m_position);
}

void Bullet::stop()
{
    m_inFlight = false;
}

bool Bullet::isInFlight()
{
    return m_inFlight;
}

sf::FloatRect Bullet::getPosition()
{
    return m_shape.getGlobalBounds();
}

sf::RectangleShape Bullet::getShape()
{
    return m_shape;
}

void Bullet::update(float elapsedTime)
{
    //Update the bullet position variables
    m_position.x += m_distanceX * elapsedTime;
    m_position.y += m_distanceY * elapsedTime;

    //Move the bullet
    m_shape.setPosition(m_position);

    //Has the bullet gone out of range?
    if(m_position.x < m_minX || m_position.x > m_maxX ||
        m_position.y < m_minY || m_position.y > m_maxY)
    {
        m_inFlight = false;
    }
}