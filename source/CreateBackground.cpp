#include <SFML/Graphics.hpp>
#include "include/main.h"

int createBackground(sf::VertexArray& r_vA, sf::IntRect arena)
{
    //How big is each tile[
    const int TILE_SIZE = 50;
    const int TILE_TYPES = 3;
    const int VERTS_IN_QUAD = 4;

    int worldWidth = arena.width / TILE_SIZE;
    int worldHeight = arena.height / TILE_SIZE;

    //Type of primitive
    r_vA.setPrimitiveType(sf::Quads);

    //Set the size of the vertex array
    r_vA.resize(worldWidth * worldHeight * VERTS_IN_QUAD);

    //Start at the beginning of the vertez array
    int currentVertex = 0;

    for(int w = 0; w < worldWidth; w++)
    {
        for(int h = 0; h < worldHeight; h++)
        {
            //Position each vertex in the current quad
            r_vA[currentVertex + 0].position = sf::Vector2f(w * TILE_SIZE, h * TILE_SIZE);

            r_vA[currentVertex + 1].position = sf::Vector2f((w * TILE_SIZE) + TILE_SIZE, h * TILE_SIZE);

            r_vA[currentVertex + 2].position = sf::Vector2f((w * TILE_SIZE) + TILE_SIZE, (h * TILE_SIZE) + TILE_SIZE);

            r_vA[currentVertex + 3].position = sf::Vector2f((w * TILE_SIZE), (h * TILE_SIZE) + TILE_SIZE);

            //Define position in texture for current quad
            if(h == 0 || h == worldHeight - 1 ||
               w == 0 || w == worldWidth - 1)
            {
                //Use the wall texture
                r_vA[currentVertex + 0].texCoords =
                    sf::Vector2f(0, 0 + TILE_TYPES * TILE_SIZE);

                r_vA[currentVertex + 1].texCoords =
                    sf::Vector2f(TILE_SIZE, 0 + TILE_TYPES * TILE_SIZE);

                r_vA[currentVertex + 2].texCoords =
                    sf::Vector2f(TILE_SIZE, TILE_SIZE + TILE_TYPES * TILE_SIZE);

                r_vA[currentVertex + 3].texCoords =
                    sf::Vector2f(0, TILE_SIZE + TILE_TYPES * TILE_SIZE);
            }
            else
            {
                //Use a random floor texture
                srand((int)time(0) + h * w + h);
                int mOrG = (rand() % TILE_TYPES);
                int verticalOffset = mOrG * TILE_SIZE;

                r_vA[currentVertex + 0].texCoords =
                    sf::Vector2f(0, 0 + verticalOffset);

                r_vA[currentVertex + 1].texCoords =
                    sf::Vector2f(TILE_SIZE, 0 + verticalOffset);

                r_vA[currentVertex + 2].texCoords =
                    sf::Vector2f(TILE_SIZE, TILE_SIZE + verticalOffset);

                r_vA[currentVertex + 3].texCoords =
                    sf::Vector2f(0, TILE_SIZE + verticalOffset);
            }

            //Position ready for the next 4 vertices
            currentVertex += VERTS_IN_QUAD;
        }
    }

    return TILE_SIZE;
}
